program ejer2;

type
	archivo_enteros = file of integer; 
	cadena = string[15];
var 	
	numeros : archivo_enteros;
	nombre : cadena;
	num, cantNum, menores : integer;
	promedio : real;
begin 
	writeln('Ingrese el nombre del archivo a abrir');
	readln(nombre);
	assign(numeros,nombre);
	reset(numeros);
	cantNum := 0; promedio := 0; menores := 0; 
	while (not eof(numeros)) do begin
		read(numeros,num);
		cantNum := cantNum + 1;
		promedio := promedio + num;
		If (num < 1500) then
			menores := menores + 1;
	end;
	promedio := (promedio / cantNum);
	close(numeros);
	writeln('La cantidad de numeros menores a 1500 es: ',menores);
	writeln('PROMEDIO: ',promedio:2:2);
end.
