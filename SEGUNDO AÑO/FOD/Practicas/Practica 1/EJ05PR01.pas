program ejer05;

type
	cadena = string[20];
	celular = record
		codigo : integer;
		nombre : cadena;
		descripcion : cadena;
		marca : cadena;
		precio : real;
		stockMinimo : integer;
		stockDisponible : integer;
	end;
	archivo_celulares = file of celular;
	
procedure crearArchivo(var archivo : archivo_celulares; var archivoText : text);
var
	celu : celular;
	nombre : cadena;
begin
	writeln('Ingrese nombre del archivo binario: ');
	readln(nombre);
	assign(archivo,nombre);
	assign(archivoText,'celulares');
	rewrite(archivo);
	reset(archivoText);
	while (not eof(archivoText)) do begin
		readln(archivoText,celu.codigo,celu.nombre,celu.descripcion,
		celu.marca,celu.precio,celu.stockMinimo,celu.stockDisponible);
		write(archivo,celu);
	end;
	close(archivo); close(archivoText);
end;

procedure imprimir (celu: celular);
begin
	writeln('---------------------');
	writeln('Codigo: ',celu.codigo);
	writeln('Nombre: ',celu.nombre);
	writeln('Descripcion: ',celu.descripcion);
	writeln('Marca: ',celu.marca);
	writeln('Precio: ',celu.precio);
	writeln('Stock minimo: ',celu.StockMinimo);
	writeln('Stock disponible: ',celu.StockDisponible);
	writeln('---------------------');
end;

procedure listarStockMinimo(var archivo : archivo_celulares);
var
	celu : celular;
begin
	reset(archivo);
	writeln('--Listado de celulares cuyo stock es menor al stock minimo--');
	while (not eof(archivo)) do begin
		read(archivo,celu);
		If (celu.stockDisponible < celu.stockMinimo) then
			imprimir(celu);
	end;
	close(archivo);
end;

procedure listarConCadena (var archivo: archivo_celulares);
var
	celu : celular;
	cad : cadena;
begin
	writeln('Ingrese cadena');
	readln(cad);
	writeln('--Listado de celulares con la cadena ingresada--');
	while (not eof(archivo)) do begin
		read(archivo,celu);
		If (celu.descripcion = cad) then
			imprimir(celu);
	end;
	close(archivo);
end;

procedure exportarTexto(var archivo: archivo_celulares; var archivoText: text);
var
	celu : celular;
begin
	assign(archivoText,'celulares');
	reset(archivo);
	rewrite(archivoText);
	while (not eof(archivo)) do begin
		read(archivo,celu);
		writeln(archivoText,'codigo: ',celu.codigo,' precio: $',celu.precio:2:2,' marca: ',celu.marca);
		writeln(archivoText,'stock disponible: ',celu.stockDisponible,' stock minimo: ',celu.stockMinimo,' descripcion: ',celu.descripcion);
		writeln(archivoText,'nombre: ',celu.nombre);
	end;
end;

var
	archivo : archivo_celulares;
	opc : integer;
	archivoText : Text;
BEGIN
	Repeat
		writeln('----- MENU -----');
		writeln('1: crear archivo celulares');
		writeln('2: listar en pantalla celulares poco stock');
		writeln('3: listar en pantalla celular con cierta cadena');
		writeln('4: exportar archivo');
		writeln('0: SALIR');
		writeln('--------------');
		readln(opc);
		case opc of
			0: writeln('SALIR');
			1: crearArchivo(archivo,archivoText);
			2: listarStockMinimo(archivo);
			3: listarConCadena(archivo);
			4: exportarTexto(archivo,archivoText);
		end;
	until (opc = 0);
END.
