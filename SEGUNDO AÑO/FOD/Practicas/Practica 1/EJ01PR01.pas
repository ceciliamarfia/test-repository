program ejer1;

const 
    corte = 30000;
type
    numeros = file of integer;
var
    enteros : numeros; nombre : string[20];
    num : integer;
begin
    writeln('Ingrese el nombre del archivo');
    readln(nombre);
    assign(enteros,nombre);
    rewrite(enteros);
    writeln('Ingrese un numero'); 
    readln(num);
    while(num <> corte) do begin
        write(enteros,num);
        writeln('Ingrese un numero');
        read(num);
    end;
    close(enteros);
end.
