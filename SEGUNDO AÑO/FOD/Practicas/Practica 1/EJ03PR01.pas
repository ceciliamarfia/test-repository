program ejer03; 

const 
	fin = 'fin';
type
	cadena = string[20];
	empleado = record
		numero : integer;
		apellido : cadena;
		nombre : cadena;
		edad : integer;
		DNI : integer;
	end;
	archivo_emp = file of empleado; 
	
procedure leerEmpleado (var emp : empleado);
begin
	writeln('Ingrese apellido del empleado');
	readln(emp.apellido);
	If (emp.apellido <> 'fin') then begin
		writeln('Ingrese el nombre del empleado');
		readln(emp.nombre);
		writeln('Ingrese el numero del empleado');
		readln(emp.numero);
		writeln('Ingrese el DNI del empleado');
		readln(emp.DNI);
		writeln('ingrese la edad del empleado');
		readln(emp.edad);
	end;
end;

procedure cargarArchivo (var archivo : archivo_emp);
var
	emp : empleado;
begin
	leerEmpleado(emp);
	while (emp.apellido <> 'fin') do begin
		write(archivo,emp);
		leerEmpleado(emp);
	end;
end;

procedure crearArchivo (var empleados : archivo_emp);
var
	nombre : cadena;
begin 
	writeln('Ingrese el nombre del archivo a crear');
	readln(nombre);
	assign(empleados,nombre);
	rewrite(empleados);
	cargarArchivo(empleados);
	close(empleados);
end;

procedure imprimirEmpleado(emp : empleado); 
begin
	writeln('Numero: ', emp.numero);
	writeln('Apellido: ', emp.apellido);
	writeln('Nombre: ',emp.nombre);
	writeln('DNI: ', emp.DNI);
	writeln('Edad: ', emp.edad);
end;

procedure listarEmpleado (var empleados : archivo_emp);
var
	buscar : cadena; emp : empleado; encontre : boolean;
begin
	encontre := false;
	writeln('Ingrese nombre o apellido a listar');
	readln(buscar);
	reset(empleados);
	while (not eof(empleados)) do begin
		read(empleados,emp);
		If ((emp.nombre = buscar) or (emp.apellido = buscar)) then
			imprimirEmpleado(emp);
			encontre := true;
	end;
	If (encontre = false) then
		writeln('No se encontro la informacion solicitada');
	close(empleados);
end;

procedure listarEmpleados(var empleados : archivo_emp);
var
	emp : empleado;
begin
	reset(empleados);
	while (not eof(empleados)) do begin
		read(empleados,emp);
		writeln(emp.numero, emp.nombre, emp.apellido, emp.edad, emp.DNI);
	end;
	close(empleados);
end;

procedure listarEmpleadoJubilaciones (var empleados : archivo_emp);
var
	emp : empleado;
begin
	reset(empleados);
	while (not eof(empleados)) do begin
		read(empleados,emp);
		If (emp.edad > 70) then
			writeln(emp.numero, emp.nombre, emp.apellido, emp.edad, emp.DNI);
	end;
	close(empleados);
end;

procedure abrirArchivo (var empleados : archivo_emp);
var
	opc : integer;
begin
	reset(empleados);
	Repeat
		writeln('-- SELECCIONAR UNA OPCION --');
		writeln('1: Listar los datos de un empleado');
		writeln('2: Listar los empleados');
		writeln('3: Listar los empleados proximos a jubilarse');
		writeln('0: Salir');
		readln(opc);
		writeln('--------------------------');
		case (opc) of
			1: listarEmpleado(empleados);
			2: listarEmpleados(empleados);
			3: listarEmpleadoJubilaciones(empleados);
			0: writeln('Has salido del menu');
		end;
	until (opc = 0);
	close(empleados);
end;
	
var
	empleados : archivo_emp;
	opc : integer;
begin
	Repeat
		writeln('----- MENU DE OPCIONES ------');
		writeln('1: crear un archivo');
		writeln('2: abrir el archivo creado');
		writeln('0: salir');
		writeln('-------------------------------');
		readln(opc);
		case opc of
			1: crearArchivo(empleados);
			2: abrirArchivo(empleados);
			0: writeln('SALIR');
		end;
	until (opc = 0);
end.
